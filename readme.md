## Overview

Traffic sampah ini adalah tools untuk ngehit sebuah url secara berulang-ulang.

## Usage
- Install phantomjs globally (or manually add bin directory to the PATH)

- Clone this repo

```
git clone git@bitbucket.org:ahmad_anshori/traffic-sampah-rumah-tangga.git
```

- Go to the directory (traffic-sampah-rumah-tangga)

```
cd traffic-sampah-rumah-tangga
```

- Run the script

```
$ ./run.sh -l 1000 -u "https://www.google.com"
```

**Options**

-l (integer) Berapa kali url akan di hit

-u (string) Target Url yang akan di hit (*required*, for safety measure, wrap url inside quotes)



