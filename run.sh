#!/bin/bash
# Basic while loop
l=1

while getopts ":l:u:" o; do
    case "${o}" in
        l)
            l=${OPTARG}
            ;;
        u)
            u=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

counter=1
while [ $counter -le $l ]
do
echo "Run: $counter"
phantomjs app.js $u
((counter++))
done
echo All done