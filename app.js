console.log('starting');
var system = require('system');
var args = system.args;
var targetUrl = '';
if (args.length === 1) {
    console.log('!!!!!!!!!! Pass a target url !!!!!!!!!!');
    phantom.exit();
} else {
    targetUrl = args[1];
}

var page = require('webpage').create();
page.customHeaders = {
    // Mobile
    // 'User-Agent': 'Mozilla/5.0 (Linux; Android 7.0; SM-G930V Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 Mobile Safari/537.36'

    // Desktop
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36'
};

// display log message when evaluating page
page.onConsoleMessage = function (msg) { console.log(msg); };

// suppress error message
page.onError = function(msg, trace) {
    var msgStack = ['ERROR: ' + msg];
    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function(t) {
            msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
        });
    }
    // uncomment to log into the console
    // console.error(msgStack.join('\n'));
};

var randTimeOnSite = Math.floor(Math.random() * 5000) + 1000;

page.open(targetUrl, function(status) {
    console.log('status: ' + status);
    page.includeJs("https://code.jquery.com/jquery-3.3.1.min.js", function() {
        setTimeout(function() {
            page.evaluate(function() {
                var ogTitle = $("meta[property='og:title']").attr("content");
                console.log('title: ' + ogTitle);
                $(".btt")[1].click();
            });
            phantom.exit()
        }, randTimeOnSite);
    });
});